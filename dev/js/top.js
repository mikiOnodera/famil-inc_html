// import 'slick-carousel'

export function mvSlide() {
  $(function(){
    // setTimeout(function(){
    //   $(".cover").fadeOut(1000);
    // },3300);
    // setTimeout(function(){
    //   $(".cover-text").fadeOut();
    // },2000);
    setTimeout(function(){
      $(".aboutUs").fadeIn(1000);
    },3300);

    $(".mvSlide").slick({
      autoplay: true,
      speed: 400,
      autoplaySpeed: 2400,
      arrows: false,
      dots: true,
      dotsClass: 'slide-dots',
      fade: true,
      initialSlide: 4,
      pauseOnFocus: false,
      pauseOnHover: false,
      pauseOnDotsHover: false
    });
  });
}