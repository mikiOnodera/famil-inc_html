export function common() {
  $(function(){

    // $(".hasSub").on("click", function(e){
    //   $(this).children('a').toggleClass('on');
    //   $(this).find('ul').toggleClass('on');
    // });

    // $(".hasSub").hover(
    //   function(){
    //     $(this).children('a').addClass('on');
    //     $(this).find('ul').addClass('on');
    //   },
    //   function(){
    //     $(this).children('a').removeClass('on');
    //     $(this).find('ul').removeClass('on');
    //   }
    // );

    if($(window).width() < 768) {
      $(".hasSub").on("click", function(){
        $(this).toggleClass('on');
      });
    } else {
      $(".hasSub")
        .mouseover(function() {
          $(this).addClass('on');
        })
        .mouseout(function() {
          $(this).removeClass('on');
      });
    }
    
    // backToTop
    $(".backToTop").hide();
    $(window).scroll(function(){
        if ($(this).scrollTop() > 200) {
          $(".backToTop").fadeIn(300);
        }else {
          $(".backToTop").fadeOut(300);
        }

        var check = window.pageYOffset;
        var dHeight = $(document).height();
        var wHeight = $(window).height();

        if(check > dHeight - wHeight - 375){
          $(".backToTop").addClass("bottom");
        }else{
          $(".backToTop").removeClass("bottom");
        }
    });
    $(".backToTop").on("click",function(){
      $("body,html").animate({scrollTop:0},500);
    });

    //hm
    $(".hm").on("click", function(){
      $(this).parent().toggleClass("open");
    });
    
    // $(".hasSuba").on("click", function() {
    //   $(this).next().slideToggle();
    // });
    $(".hasSuba").on("click", function() {
      $(this).next("ul").slideToggle();
    });

  });
}
export function anime() {
  const target = document.getElementsByClassName('anime');
  const position = Math.floor(window.innerHeight * .75);
  for (let i = 0; i < target.length; i++) {
      let offsetTop = Math.floor(target[i].getBoundingClientRect().top);
      if (offsetTop < position) {
          target[i].classList.add('animeOn');
      }
  }
}