const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: "development",
  entry: `./dev/js/index.js`,
  output: {
    filename: "main.js"
  },
  module: {
    rules: [
      {
        test: /.(jpg|png|gif|svg)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '/image/[name].[ext]',
          }
        }
      },
      {
        test: /\.scss$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: {
              url: true,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                outputStyle: 'expanded',
              },
            },
          },
        ]
      },
    ]
  },
  devServer: {
    static: "dist",
    open: true
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),
    new MiniCssExtractPlugin({
			filename: 'css/style.css',
			ignoreOrder: true,
		}),
  ],
};